# SmartRecruiters JIRA email cleaner #

A Chrome extension that cleans up the html description created by JIRA Service Desk from incoming SmartRecruiters emails.

The SmartRecruiters emails are sent in html format, which doesn't render well in JIRA's description fields.

# How to run
Checkout the repo and add it as unpacked extension to Chrome.