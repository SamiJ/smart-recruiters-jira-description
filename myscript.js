(function () {
    if (window.JIRA && window.JIRA.bind) {
        console.log('SRD - INIT');
        jQuery(document).ajaxComplete(transformSmartRecruitersHtmlDescription);
    }

    function transformSmartRecruitersHtmlDescription() {
        const $description = jQuery("#description-val .user-content-block");
        if ($description.length && $description.text().indexOf("SmartRecruiters") !== -1 && !$description.data("convertedToHtml")) {
            console.log('SRD - starting transformation');
            $description.data("convertedToHtml", true);

            function htmlize($root) {
                const candidateLink = $root.text()
                    .replace(/[^]*href="(.*?)"[^]*/, "$1") // get the contents of href from root text
                    .replace(/%/g,'%25'); // the path contains % characters, encode them here.

                let text = $root.text();
                text = text.replace("PeopleTeamQueries GenericUser", "");
                text = text.replace("Reply to this email to message the sender", "");
                text = text.replace(/\<br\>/gi, "\n");
                text = text.replace(/\n\n\n\n/gi, "\n");
                text = text.replace(/(<\/([^>]+)>)/ig, "\n");
                text = text.replace(/(<([^>]+)>)/ig, "");
                text = text.replace(/\u00a0/ig, ""); // replace &nbsp;

                $root.css("white-space", "pre");

                $root.text(text.trim() + "\n\n");

                jQuery("<a />")
                    .attr({
                        href: candidateLink,
                        rel: 'noreferrer'
                    })
                    .text("View Candidate Profile")
                    .appendTo($root)
            }

            htmlize($description);
            console.log('SRD - transformation complete');
        }
    }
})();
